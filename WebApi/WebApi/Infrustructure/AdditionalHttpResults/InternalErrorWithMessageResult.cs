﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebApi.Infrustructure.AdditionalHttpResults
{
    public class InternalErrorWithMessageResult : IHttpActionResult
    {
        private readonly string message;

        public InternalErrorWithMessageResult(string message)
        {
            this.message = message;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                Content = new StringContent(message),
                StatusCode = HttpStatusCode.InternalServerError
            };

            return Task.FromResult(response);
        }
    }
}