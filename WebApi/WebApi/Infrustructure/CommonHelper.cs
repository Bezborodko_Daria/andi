﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Infrustructure
{
    public class CommonHelper
    {
        private const int _keyLenght = 8;
        public static string GetSecret()
        {
            return Guid.NewGuid().ToString().Replace("-", "").Substring(0, _keyLenght);
        }
    }
}