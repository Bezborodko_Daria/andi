﻿using System;
using System.Collections.Generic;
using System.Web.Helpers;
using WebApi.Database;
using WebApi.Database.Repository;
using WebApi.Infrustructure;
using WebApi.Models;

namespace WebApi.Services
{
    public class LicenseService
    {
        #region Queries
        
        /// <summary>
        /// Check if license is free
        /// </summary>
        /// <param name="licenseKey">String license key</param>
        /// <returns>Result of free property</returns>
        public bool IsFree(string licenseKey)
        {
            using (var repo = new LicenseRepository( new Workshop()))
            {
                return repo.GetLicense(licenseKey).IsUsed == false;
            }
        }

        #endregion


        /// <summary>
        /// Validating license and if valid, it'll set as used 
        /// </summary>
        /// <param name="secretKey">String secret key</param>
        public void ConfirmRegistration(string secretKey)
        {
            if (string.IsNullOrEmpty(secretKey))
                throw new NullReferenceException("Secret key is required!");

            var context = new Workshop();
            using (var userRepo = new UserRepository(context))
            {
                using (var licenseRepo = new LicenseRepository(context))
                {
                    var user = userRepo.GetUser(secretKey, By.SecretKey);

                    if (user == null) throw new NullReferenceException(nameof(UserProfile));
                    if (!user.License.IsUsed)
                    {
                        licenseRepo.SetAsUsed(user.License);
                        userRepo.ConfirmRegistration(user);
                    }
                    else throw new Exception("License has already used!");
                }
            }
        }

        /// <summary>
        /// Register user with confirming message  if all valid
        /// </summary>
        /// <param name="model"><see cref="RegisterUserModel"/></param>
        public void RegistrationUserByLicense(RegisterUserModel model)
        {
            if (model == null) throw new NullReferenceException(nameof(RegisterUserModel));
            if (IsFree(model.LicenseKey))
            {
                model.Password = Crypto.HashPassword(model.Password);
                using (var repo = new UserRepository(new Workshop()))
                {
                    var user = UserProfile.Map(model);
                    user.Id = Guid.NewGuid().ToString();
                    user.SecretKey = CommonHelper.GetSecret();
                    repo.AddUser(user);
                    EmailSender.Send(model.Email, "http://localhost:52726/api/Account/ConfirmRegistration?key=" + user.SecretKey, "Confirmation");
                }
               
            }
            else throw new Exception("Licence key is already exist!");
        }

        public void Generate()
        {
            var licenses = new List<License>();
            for (int i = 0; i < 10; i++)
            {
                licenses.Add(new License { Key = Guid.NewGuid().ToString(), IsUsed = false });
            }
            using (var repo = new LicenseRepository(new Workshop()))
            {
                repo.AddLicenses(licenses);
            }
        }
    }
}