using MySql.Data.Entity;

namespace WebApi.Database
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class Workshop : DbContext
    {
        // Your context has been configured to use a 'Workshop' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'WebApi.Database.Workshop' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'DefaultConnection' 
        // connection string in the application configuration file.
        public Workshop()
            : base("name=DefaultConnection")
        {
        }

        public virtual DbSet<License> Licenses { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
    }


}