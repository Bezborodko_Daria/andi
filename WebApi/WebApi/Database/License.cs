﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi.Database
{
    public class License
    {
        [Key]
        public string Key { get; set; }
        public bool IsUsed { get; set; }

        public virtual DbSet<UserProfile> UserProfiles { get; set; }
    }
}