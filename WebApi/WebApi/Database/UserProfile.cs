﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.Database
{
    public class UserProfile
    {
        [Key]
        public string Id { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string PasswordHash { get; set; }
        public string SecretKey { get; set; }
        public DateTime? DateConfirmed { get; set; }
        public string LicenseId { get; set; }

        [ForeignKey("LicenseId")]
        public virtual License License { get; set; }

        internal static UserProfile Map(RegisterUserModel model)
        {
            return new UserProfile
            {
                Email = model.Email,
                Firstname = model.Firstname,
                Lastname = model.Lastname,
                LicenseId = model.LicenseKey,
                PasswordHash = model.Password
            };

        }
    }
}