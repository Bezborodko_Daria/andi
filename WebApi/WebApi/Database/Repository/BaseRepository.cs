﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Database.Repository
{
    public class BaseRepository : IDisposable
    {

        protected Workshop _db;
        private bool disposedValue = false;

        public BaseRepository(Workshop context)
        {
            _db = context;
        }
       
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                disposedValue = true;
            }
        }


        public void Dispose()
        {
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

    }
}