﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Infrustructure;

namespace WebApi.Database.Repository
{
    public class UserRepository : BaseRepository
    {
        public UserRepository(Workshop context) : base(context) { }
        
        #region Queries

        public UserProfile GetUser(string Id, By by)
        {
            switch (by)
            {
                case By.Id:
                    return _db.UserProfiles.FirstOrDefault(i => i.Id.Equals(Id));
                case By.SecretKey:
                    return _db.UserProfiles.FirstOrDefault(i => i.SecretKey.Equals(Id));
                case By.Email:
                    return _db.UserProfiles.FirstOrDefault(i => i.Email.Equals(Id));
                default:
                    throw new NotImplementedException();
            }
        }

        #endregion

        #region Commands

        public void AddUser(UserProfile user)
        {
            try
            {
                _db.UserProfiles.Add(user);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ConfirmRegistration(UserProfile user)
        {
            try
            {
                user.DateConfirmed = DateTime.Now;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


    }
}