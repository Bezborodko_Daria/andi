﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Database.Repository
{
    public class LicenseRepository : BaseRepository
    {

        public LicenseRepository(Workshop context) : base (context) {   }

        #region Queries

        public License GetLicense(string key)
        {
            return _db.Licenses.FirstOrDefault(i => i.Key.Equals(key));
        }

        #endregion

        #region Commands

        public void SetUsed(string key)
        {
            SetAsUsed(_db.Licenses.First(i => i.Key.Equals(key)));
        }

        public void SetAsUsed(License license)
        {
            try
            { 
                license.IsUsed = true;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void SetFree(string key)
        {
            SetFree(_db.Licenses.First(i => i.Key.Equals(key)));
        }

        public void SetFree(License license)
        {
            try
            {
                license.IsUsed = false;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

        }

        public void AddLicence(License license)
        {
            try
            {
                _db.Licenses.Add(license);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }

        public void AddLicenses(List<License> licenses)
        {
            try
            {
                _db.Licenses.AddRange(licenses);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }

        #endregion
    }
}