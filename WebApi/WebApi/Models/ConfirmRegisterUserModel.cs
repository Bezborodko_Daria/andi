﻿namespace WebApi.Models
{
    /// <summary>
    /// Model for email confirming
    /// </summary>
    public class ConfirmRegisterUserModel
    {
        public string SecretKey { get; set; }
    }
}