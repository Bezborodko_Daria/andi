﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Database;

namespace WebApi.Models
{
    public class RegisterUserModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string LicenseKey { get; set; }


        public static UserProfile Map(RegisterUserModel model)
        {
            return new UserProfile
            {
                Email = model.Email,
                Firstname = model.Firstname,
                Lastname = model.Lastname,
                LicenseId = model.LicenseKey,
                PasswordHash = model.Password
            };
        }
    }
}