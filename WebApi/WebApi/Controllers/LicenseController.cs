﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Infrustructure.AdditionalHttpResults;
using WebApi.Services;

namespace WebApi.Controllers
{
    public class LicenseController : ApiController
    {
        private readonly LicenseService licenseService;

        public LicenseController()
        {
            licenseService = licenseService ?? new LicenseService();
        }

        /// <summary>
        /// (API) Generate random licenses
        /// </summary>
        /// <returns><see cref="IHttpActionResult"/></returns>
        [HttpGet]
        public IHttpActionResult Generate()
        {
            try
            {
                licenseService.Generate();
                return Ok();
            }
            catch (Exception ex)
            {
                return new InternalErrorWithMessageResult(ex.Message);
            }

        }
    }
}
