﻿using System;
using System.Web.Http;
using WebApi.Infrustructure.AdditionalHttpResults;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    public class AccountController : ApiController
    {
        private readonly LicenseService licenseService;
        public AccountController()
        {

            licenseService = licenseService ?? new LicenseService();
        }

        /// <summary>
        /// (API) Registration api ctrl
        /// </summary>
        /// <param name="model"><see cref="RegisterUserModel"/></param>
        /// <returns><see cref="IHttpActionResult"/></returns>
        [HttpPost]
        public IHttpActionResult Registration(RegisterUserModel model)
        {
            try
            {
                licenseService.RegistrationUserByLicense(model);
                return Ok();
            }
            catch (Exception ex)
            {
                return new InternalErrorWithMessageResult(ex.Message);
            }
        }


        /// <summary>
        /// (API) Confirm registration from email
        /// </summary>
        /// <returns><see cref="IHttpActionResult"/></returns>
        [HttpGet]
        public IHttpActionResult ConfirmRegistration(string key)
        {
            try
            {
                licenseService.ConfirmRegistration(key);
                return Ok();
            }
            catch (Exception ex)
            {
                return new InternalErrorWithMessageResult(ex.Message);
            }

        }
    }
}
